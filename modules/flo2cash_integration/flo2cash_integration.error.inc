<?php

function flo2cash_integration_error ($token) {
  $o = new stdClass();
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (drupal_valid_token($token) ) {
      $json = $_POST['error'];
      watchdog ('flo2cash_api', 'flo2cash iframe error: @json', array ('@json' => json_encode($json)), WATCHDOG_NOTICE);
    }
  }
  drupal_json_output($o);
}

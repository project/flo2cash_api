<?php

/**
 * @file
 * Admin page callback file for the flo2cash_api module.
 */

/**
 * Form builder; Configure flo2cash credentials for this site.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function flo2cash_api_admin_settings() {
  $form['flo2cash_api_account_id'] = array(
    '#type' => 'textfield',
    '#title' => t ('Account id'),
    '#description' => t ('Also called Sub Account Id'),
    '#size' => 12,
    '#default_value' => variable_get('flo2cash_api_account_id', ''),
  );
  $form['flo2cash_api_user_name'] = array(
    '#type' => 'textfield',
    '#title' => t ('User name'),
    '#description' => t ('User credentials for the Payment/Mobile/Credit Card/Direct Debit Web Services. Also known as Id, Merchant Id or Client Id.'),
    '#size' => 12,
    '#default_value' => variable_get('flo2cash_api_user_name', ''),
  );
  $form['flo2cash_api_password'] = array(
    '#type' => 'password',
    '#title' => t ('Password'),
    '#description' => t ('Password for the Payment/Mobile/Credit Card/Direct Debit Web Services.'),
    '#size' => 12,
    '#default_value' => variable_get('flo2cash_api_password', ''),
  );
  $form['flo2cash_authorisation'] = array(
    '#type' => 'textfield',
    '#title' => t ('Direct post authorisation'),
    '#description' => t ('Optional: the Authorisation Header Value when using embedded integration. Also known as API Key. Value can be obtained by logging into the flo2cash merchant console, and then going to Channel Settings > Web Payments > Direct Post Settings.'),
    '#size' => 64,
    '#default_value' => variable_get('flo2cash_authorisation', ''),
  );
  $form['flo2cash_api_is_demo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Demo environment.'),
    '#default_value' => variable_get('flo2cash_api_is_demo', FALSE),
    '#description' => t('Check during development to allow test accounts and test cards to be used..')
  );

  return system_settings_form($form);
}
